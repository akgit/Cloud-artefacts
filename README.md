This is simple AWS Codepipeline implementation using all steps starting from image build, testing and actual deployment into ECS.

Fully working pipeline which

1. Integrates with GitHub where all application, as well as pipeline, docker code is stored.
2. Based on commit to branch, tiggers pipieline.
3. Builds, tests and deploys container into ECS

